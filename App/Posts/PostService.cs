using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MiniConnector;
using MiniSocial.Posts;
using SocialSharing.Data.Sources;
using SocialSharing.Data.Users;

namespace SocialSharing.App.Posts
{
    public class PostService
    {
        public async Task<IEnumerable<IPost>> GetPosts(User user)
        {
            IEnumerable<Task<IEnumerable<IPost>>> tasks = new SourceService()
                .GetAll()
                .Select(source => CallService(source.Platform.ToString(), $"posts/{source.Query}"));

            tasks = RunEasterEgg(tasks, user);

            await Task.WhenAll(tasks);
            
            IEnumerable<IPost> posts = await MergeResults(tasks);
            posts = posts.OrderByDescending(post => post.CreationDate);

            return posts;
        }

        private IEnumerable<Task<IEnumerable<IPost>>> RunEasterEgg(IEnumerable<Task<IEnumerable<IPost>>> tasks, User user)
        {
            if (user?.Theme == "trump")
            {
                tasks = new List<Task<IEnumerable<IPost>>>() { CallService(Services.REDDIT, "posts/TinyTrumps") };
            }

            if (user?.Theme == "birbs")
            {
                tasks = new List<Task<IEnumerable<IPost>>>() { CallService(Services.REDDIT, "posts/birdswitharms") };
            }

            return tasks;
        }

        private async Task<IEnumerable<IPost>> CallService(string service, string url)
        {
            IEnumerable<IPost> posts = new List<IPost>();
            try
            {
                posts = await MicroService.Call<IEnumerable<Post>>(service, url);
            }
            catch {}

            return posts;
        }

        private async Task<IEnumerable<IPost>> MergeResults(IEnumerable<Task<IEnumerable<IPost>>> results)
        {
            List<IPost> posts = new List<IPost>();

            foreach (Task<IEnumerable<IPost>> r in results)
            {
                posts.AddRange(await r);
            }

            return posts;
        }
    }
}