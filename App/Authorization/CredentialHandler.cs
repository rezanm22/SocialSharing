using System;
using System.Diagnostics;
using System.Text;
using Microsoft.AspNetCore.Http;

namespace SocialSharing.App.Authorization
{
    public class CredentialService
    {
        public Credentials GetCredentials(HttpRequest request)
        {
            request.Headers.TryGetValue("Authorization", out var authenticationValues);
            string authentication = authenticationValues.ToString();

            if (string.IsNullOrEmpty(authentication) ||
                !authentication.StartsWith(BearerAuthenticationOptions.DefaultScheme))
            {
                return null;
            }

            return ParseCredentials(authentication.Substring(6));
        }

        private Credentials ParseCredentials(string token)
        {
            byte[] data = Convert.FromBase64String(token);
            string credentials = Encoding.UTF8.GetString(data);

            int colonIndex = credentials.IndexOf(':');

            return new Credentials(credentials.Substring(0, colonIndex), credentials.Substring(colonIndex + 1));
        }
    }
    public class Credentials
    {
        public string Username { get; set; } = "";
        public string Password { get; set; } = "";

        public Credentials(string username = null, string password = null)
        {
            this.Username = username;
            this.Password = password;
        }
    }
}