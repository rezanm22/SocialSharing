using System.Collections.Generic;

namespace SocialSharing.Data.Sources
{
    internal class SourceDao : Dao
    {
        public Source Get(int id)
        {
            return Execute(database => FindEntity(database.Sources, s => s.Id == id));
        }

        public IEnumerable<Source> GetAll()
        {
            return Execute(database => FindEntities(database.Sources, s => true));
        }

        public Source Save(Source source)
        {
            return Execute(database =>
            {
                AddOrUpdateEntity(source, database.Sources, s => s.Id == source.Id);
                database.SaveChanges();
                return source;
            });
        }

        public void Delete(int id)
        {
            Execute(database =>
            {
                DeleteEntity(id, database.Sources, s => s.Id == id);
                database.SaveChanges();
            });
        }
    }
}