﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using MiniSocial;
using MiniSocial.Authentication;
using MiniSocial.Posts;

namespace SocialSharing.TwitterMicro.Controllers
{
    [Route("api/posts")]
    public class PostController : Controller
    {
        private Dictionary<string, string> credentials;

        public PostController(IConfiguration configuration)
        {
            credentials = new Dictionary<string, string>();
            credentials.Add(FacebookAuthenticator.APP_ID, configuration["Keys:Facebook:AppId"]);
            credentials.Add(FacebookAuthenticator.APP_SECRET, configuration["Keys:Facebook:AppSecret"]);
        }

        [HttpGet("{query}")]
        public async Task<IEnumerable<IPost>> Get(string query)
        {
            return await new MiniPosts().GetPost(new PostRequest(Platforms.FACEBOOK, query, authenticationProperties: credentials));
        }
    }
}
