import { LitElement, html } from '@polymer/lit-element';
export { SocialUserEdit };

import { SocialRequest} from './social-request';
import '@polymer/paper-input/paper-input.js';
import '@polymer/paper-button/paper-button.js';

class SocialUserEdit extends LitElement {
  constructor() {
    super();
    window.addEventListener('user', (e) => this.restoreValues());
  }

  _render() {
    return html`  
    <style>
        .user-input {
          display: flex;
          flex-direction: column;
          max-width: 300px;
          margin: 0 auto;
        }

        paper-button {
          display: flex;
          flex-direction: column;
          margin-top: 15px;
        }

    </style>
    <div class="user-input">
        <paper-input label="mail" id="mail" auto-validate pattern="[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}" error-message="Enter valid mail address" disabled></paper-input>
        <paper-input label="name*" id="displayname"></paper-input>
        <paper-input label="theme (leave empty for default)" id="theme"></paper-input>
        <paper-input label="password*"type="password" id="password" on-keyup="${(e) => this.validatePassword(e)}" auto-validate pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).{6,256}$" error-message="Password must contain uppercase, lowercase and a number."></paper-input>
        <paper-button raised id="editAccountButton" disabled="true" on-click="${() => this.send()}">edit</paper-button>
        <paper-input label="account delete (email)" id="accountdelete" on-keyup="${(e) => this.validateEmail(e)}" auto-validate pattern="[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}" error-message="Enter valid mail address"></paper-input>
        <paper-button raised id="deleteButton" on-click="${() => this.delete()}" disabled="true">delete</paper-button> 
    </div>
    <social-request id="request" url="users" method="POST" on-response="${(e) => this.editResponse(e)}"></social-request>
    <social-request id="deleteRequest" url="users" method="DELETE" on-response="${(e) => this.onResponse(e)}"></social-request>
    `;
  }

  _didRender() {
    this.restoreValues();
  }

  editResponse(e){
    const mail = this.shadowRoot.getElementById('mail').value;
    const password = this.shadowRoot.getElementById('password').value;
    window.dispatchEvent(new CustomEvent('login', {detail: { mail: mail, password: password }}));
    this.shadowRoot.getElementById('password').value = '';
  }
  onResponse(e) {
    const response = e.detail.response;
    window.user = response;
    window.dispatchEvent(new CustomEvent('logout'));
    window.location.href = '/login';
  }

  validatePassword(e) {
    const pass = this.shadowRoot.getElementById('password').value;
    const valid = this.isValidPassword(pass);
    this.shadowRoot.getElementById('editAccountButton').disabled = valid;
    if(this._confirmEnterButton(e) && valid === false) {
      this.send()
    }
  }

  validateEmail(e) {
    const email = this.shadowRoot.getElementById('mail').value;
    const deleteEmail = this.shadowRoot.getElementById('accountdelete').value;
    if(email === deleteEmail) {
      this.shadowRoot.getElementById('deleteButton').disabled = false;
    } else {
      this.shadowRoot.getElementById('deleteButton').disabled = true;
    }

    if(this._confirmEnterButton(e) && email === deleteEmail){
      this.delete();
    }
  }

  check(iets){
    if(iets == "kaas"){
      return true;
    }
    else if(iets ==  null){
      return false;
    }
  }

  isValidPassword(password) {
    var pattern = "^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).{6,256}$";
    const matches = password.match(new RegExp(pattern));
    if(matches != null) { 
      return false;
    } else {
      return true;
    }
  }

  send() {
      const request = this.shadowRoot.querySelector("#request");
      const mail = this.shadowRoot.getElementById('mail').value;
      const name = this.shadowRoot.getElementById('displayname').value;
      const password = this.shadowRoot.getElementById('password').value;
      const theme = this.shadowRoot.getElementById('theme').value;

      request.body = {
          mail: mail,
          name: name,
          password: password,
          theme: theme,
          is_manager: window.user.is_manager
      };    
      request.updateAndSend();
      this.showAccountCreatedMessage();
  }

  delete() {
    const deleteRequest = this.shadowRoot.querySelector("#deleteRequest");
    const deleteMail = this.shadowRoot.getElementById('accountdelete').value;
    const mail = this.shadowRoot.getElementById('mail').value;
    if (mail === deleteMail) {
      deleteRequest.url = `users/${encodeURI(mail)}`;
      deleteRequest.updateAndSend();
    } else {
      this.showWrongEmailError();
    }
  }

  restoreValues() {
    if (window.user == null || !this.shadowRoot) return;
    this.shadowRoot.getElementById('mail').value = window.user.mail;
    this.shadowRoot.getElementById('displayname').value = window.user.name;
    this.shadowRoot.getElementById('theme').value = window.user.theme;
    
  }

  showAccountCreatedMessage() {
    window.dispatchEvent(new CustomEvent('show-toast', { detail: { message: 'Account edited' }}));
  }

  showWrongEmailError() {
    window.dispatchEvent(new CustomEvent('show-toast', { detail: { message: 'Emails did not match' }}));
    this.shadowRoot.getElementById('accountdelete').value = '';
  }

  _confirmEnterButton(e){
    return e.keyCode === 13;
  }
}

window.customElements.define('social-user-edit', SocialUserEdit);
