import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
export { SocialRegister };

import { SocialUserForm} from './social-user-form';


class SocialRegister extends PolymerElement {

    static get template() {
        return html`
        <social-user-form></social-user-form>
        `;
    }

    static get is() {return 'social-register'}
}

window.customElements.define(SocialRegister.is, SocialRegister);