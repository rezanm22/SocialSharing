import { LitElement, html } from '@polymer/lit-element';
export { SocialRecover };

import '@polymer/paper-input/paper-input.js';
import '@polymer/paper-button/paper-button.js';
import '@polymer/iron-selector/iron-selector.js';
import '@polymer/app-route/app-route.js';

class SocialRecover extends LitElement {

  static get properties() {
    return {};
  }

  _render({}) {
    return html`
      <div class="container">
        <paper-input id="emailaddress" label="email address" autofocus auto-validate pattern="[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}" error-message="Enter valid mail address"></paper-input>
        <a name="recover password"><paper-button id="recover" on-click="${() => this.recoverPassword()}">recover password</paper-button></a>
      </div>
      <social-request id="recoverpassword" url="users/recover" on-response=${() => this.onResponse()}" method="GET"><social-request>

      <style>
        .container {
          display: flex;
          flex-direction: column;
          max-width: 250px;
          margin: 0 auto;
        }

        .container a {
          text-decoration: none;
          color: var(--text-color);
          display: flex;
          flex-direction: column;
        } 
      </style>
    `;
  }

  recoverPassword() {
      const recoverMail = this.shadowRoot.getElementById('emailaddress').value;
      const recoverRequest = this.shadowRoot.querySelector('#recoverpassword');
      console.log(recoverMail);
      recoverRequest.url = `users/recover/${encodeURI(recoverMail)}`;
      recoverRequest.updateAndSend();
    }

    onResponse() {
      window.dispatchEvent(new CustomEvent('show-toast', { detail: { message: `Mail has been sent to ${this.shadowRoot.getElementById('emailaddress').value}` }}));

    }
}

window.customElements.define('social-recover', SocialRecover);
