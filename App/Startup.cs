﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using SocialSharing.App;
using SocialSharing.App.Authorization;
using SocialSharing.Data.Mail;

namespace SocialSharing
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();
            
            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = BearerAuthenticationOptions.DefaultScheme;
                options.DefaultChallengeScheme = BearerAuthenticationOptions.DefaultScheme;
            }).AddBearerAuthentication(options => new BearerAuthenticationOptions());

            services.AddMvc(options =>
            {
                options.Filters.Add(new AuthorizeFilter(new AuthorizationPolicyBuilder()
                    .AddAuthenticationSchemes(BearerAuthenticationOptions.DefaultScheme)
                    .RequireAuthenticatedUser()
                    .Build()));
            });

            services.AddAuthorization(options => 
            {
                options.AddPolicy(Policy.IsManager, policy => policy.RequireClaim(ClaimType.IsManager));
            });

            MailService.MailConfiguration = Configuration.GetSection("MailConfiguration").Get<MailConfiguration>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCors(p => p
                .AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader());

            app.Use(async (context, next) =>
            {
                await next();
                if (context.Response.StatusCode == 404 &&
                    !Path.HasExtension(context.Request.Path.Value) &&
                    !context.Request.Path.Value.StartsWith("/api/"))
                {
                    context.Request.Path = "/index.html";
                    await next();
                }
            });

            app.UseMvcWithDefaultRoute();
            app.UseDefaultFiles();
            app.UseStaticFiles();

            MiniConnector.MicroService.Add(Services.TWITTER, Services.TWITTER_URL);
            MiniConnector.MicroService.Add(Services.FACEBOOK, Services.FACEBOOK_URL);
            MiniConnector.MicroService.Add(Services.REDDIT, Services.REDDIT_URL);
        }
    }
}
