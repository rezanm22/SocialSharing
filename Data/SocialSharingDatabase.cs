using System.IO;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json.Linq;
using SocialSharing.Data.Sources;
using SocialSharing.Data.Users;

namespace SocialSharing.Data
{
    public class SocialSharingDatabase : DbContext
    {
        private readonly string[] ConfigurationKeys = new string[]
        {
            "Host",
            "Port",
            "Username",
            "Password",
            "Database"
        };
        public DbSet<User> Users { get; set; }
        public DbSet<Source> Sources { get; set; }
        
        protected override void OnConfiguring(DbContextOptionsBuilder builder)
        {
            if (!builder.IsConfigured)
            {
                builder.UseNpgsql(this.GetConnectionString());
            }
        }

        private string GetConnectionString()
        {
            JObject json = JObject.Parse(File.ReadAllText(Path.Combine(Directory.GetCurrentDirectory(), "settings.json"))).GetValue("Database") as JObject;

            string result = "";

            foreach (string key in this.ConfigurationKeys)
            {
                result += $"{key}={json[key]};";
            }

            return result;
        }
    }
}