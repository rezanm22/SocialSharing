import { LitElement, html } from '@polymer/lit-element';
export { SocialUserForm };

import { SocialRequest} from './social-request';
import '@polymer/paper-input/paper-input.js';
import '@polymer/paper-button/paper-button.js';

class SocialUserForm extends LitElement {
  _render() {
    return html`  
    <style>
        .user-input {
            max-width: 400px;
            margin: 0 auto;
            display: flex;
            flex-direction: column;
        }

        paper-button {
            align-self: center;
            margin: var(--spacing);
        }
    </style>
    <div class="user-input">
        <paper-input label="mail*" id="mail" auto-validate pattern="[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}" error-message="Enter valid mail address"></paper-input>
        <paper-input label="name*" id="displayname"></paper-input>
        <paper-input label="password*" type="password" id="password" auto-validate pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).{6,256}$" error-message="Password must contain uppercase, lowercase and a number."></paper-input>
        <paper-input label="repeat password*" type="password" id="repeatpassword" on-keydown="${(e) => this.keyPressed(e)}" auto-validate pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).{6,256}$" error-message="Password must contain uppercase, lowercase and a number."></paper-input>
        <paper-button raised on-click="${() => this.send()}">create account</paper-button>
    </div>
    <social-request id="request" url="users" on-response="${() => this.onResponse()}" method="POST"></social-request>
    `;
  }

  send() {
      const request = this.shadowRoot.querySelector("#request");
      const mail = this.shadowRoot.getElementById('mail').value;
      const name = this.shadowRoot.getElementById('displayname').value;
      const password = this.shadowRoot.getElementById('password').value;
      const repeatpassword = this.shadowRoot.getElementById('repeatpassword').value;

      request.body = {
          mail: mail,
          name: name,
          password: password
      };   

      if(password === repeatpassword) {
        request.updateAndSend();
        // this.showAccountCreatedMessage();
      } else {
          this.showPasswordError();
      }
      
  }

  onResponse() {
    const mail = this.shadowRoot.getElementById('mail').value;
    const password = this.shadowRoot.getElementById('password').value;
    window.dispatchEvent(new CustomEvent('login', {detail: { mail: mail, password: password }}));
    window.location.href = '/';
  }

  showAccountCreatedMessage() {
    window.dispatchEvent(new CustomEvent('show-toast', { detail: { message: 'Account created YEET!' }}));
  }

  showPasswordError() {
    window.dispatchEvent(new CustomEvent('show-toast', { detail: { message: 'Passwords did not match' }}));
    this.shadowRoot.getElementById('password').value = '';
    this.shadowRoot.getElementById('repeatpassword').value = '';
  }

  keyPressed(e){
    if(13 === e.keyCode) {
      this.send();
    }
  }
}

window.customElements.define('social-user-form', SocialUserForm);
