using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SocialSharing.Data.Users;

namespace SocialSharing.Tests.Users
{
    public abstract class UserTest
    {
        protected List<User> CreatedUsers;
        protected UserService Service;
        
        [TestInitialize]
        public void Init()
        {
            Service = new UserService();
            CreatedUsers = new List<User>();
        }

        [TestCleanup]
        public void Teardown()
        {
            CreatedUsers
                .Where(u => u != null)
                .ToList()
                .ForEach(u => Service.Delete(u.Mail));
        }
    }
}