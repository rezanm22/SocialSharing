using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace SocialSharing.Data.Sources
{
    public class Source
    {
        [Key]
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("platform")]
        [JsonConverter(typeof(StringEnumConverter))]
        public Platform Platform { get; set; }

        [JsonProperty("query")]
        public string Query { get; set; }
    }

    public enum Platform
    {
        TWITTER,
        FACEBOOK,
        LINKEDIN,
        INSTAGRAM,
        REDDIT
    }
}