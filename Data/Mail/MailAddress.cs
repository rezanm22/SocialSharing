namespace SocialSharing.Data.Mail
{
    public class MailAddress
    {
        public string Name { get; set; }
        public string Mail { get; set; }
    }
}