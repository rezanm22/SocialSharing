using System;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace SocialSharing.App.Authorization
{
    public class AuthenticationExceptionFilterAttribute : ExceptionFilterAttribute
    {
        public override void OnException(ExceptionContext context)
        {
            if (context.ExceptionHandled) return;
            var result = new ViewResult { ViewName = "ApplicationError" };
            context.ExceptionHandled = true;
            context.HttpContext.Response.Body.Flush();
            context.Result = result;
        }
    }
}