import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
export { SocialSettings };
import './social-user-edit';

import { SocialUserForm} from './social-user-form';

class SocialSettings extends PolymerElement {
    
    static get template() {
        return html`
        ${this.styles}
        <social-user-edit></social-user-edit>
        `;
    }

    static get styles() {
        return html `
        `;
    }

    static get is() {return 'social-settings'; }
}

window.customElements.define(SocialSettings.is, SocialSettings);