﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using MiniSocial;
using MiniSocial.Authentication;
using MiniSocial.Posts;

namespace SocialSharing.TwitterMicro.Controllers
{
    [Route("api/posts")]
    public class PostController : Controller
    {
        private Dictionary<string, string> credentials;

        public PostController(IConfiguration configuration)
        {
            credentials = new Dictionary<string, string>();
            credentials.Add(TwitterAuthenticator.CONSUMER_KEY, configuration["Keys:Twitter:ConsumerKey"]);
            credentials.Add(TwitterAuthenticator.CONSUMER_SECRET, configuration["Keys:Twitter:ConsumerSecret"]);
            credentials.Add(TwitterAuthenticator.OAUTH_TOKEN, configuration["Keys:Twitter:OAuthKey"]);
            credentials.Add(TwitterAuthenticator.OAUTH_SECRET, configuration["Keys:Twitter:OAuthSecret"]);
        }

        [HttpGet("{query}")]
        public async Task<IEnumerable<IPost>> Get(string query)
        {
            return await new MiniPosts().GetPost(new PostRequest(Platforms.TWITTER, query, authenticationProperties: credentials));
        }

        [HttpPut("like/{id}")]
        public async Task LikePost(string id)
        {
            Console.WriteLine("Yee boii u like dis");
        }
    }
}
