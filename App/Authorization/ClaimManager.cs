using System.Collections.Generic;
using System.Security.Claims;
using SocialSharing.Data.Users;

namespace SocialSharing.App.Authorization
{
    public class ClaimManager
    {
        public static IEnumerable<Claim> GetClaims(User user)
        {
            List<Claim> claims = new List<Claim>();

            if (user != null)
            {
                claims.Add(new Claim(ClaimType.Mail, user?.Mail, ClaimValueTypes.String));
            }

            if (user != null &&
                user.IsManager)
            {
                claims.Add(new Claim(ClaimType.IsManager, user.IsManager.ToString(), ClaimValueTypes.Boolean));
            }

            return claims;
        }
    }
}