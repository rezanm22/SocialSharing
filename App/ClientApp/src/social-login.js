import { LitElement, html } from '@polymer/lit-element';
export { SocialLogin };

import '@polymer/paper-input/paper-input.js';
import '@polymer/paper-button/paper-button.js';
import '@polymer/iron-selector/iron-selector.js';
import '@polymer/app-route/app-route.js';

class SocialLogin extends LitElement {

  static get properties() {
    return {};
  }

  _render({}) {
    return html`
      <div class="container">
        <paper-input id="mail" label="mail" autofocus></paper-input>
        <paper-input id="password" label="password" type="password" on-keydown="${(e) => this.keyPressed(e)}"></paper-input>
        <a name="login" id="login" href="feed"><paper-button raised on-click="${() => this.login()}">login</paper-button></a>
        <a name="register" href="register"><paper-button>register</paper-button></a>
        <a name="recover password" href="recover"><paper-button>recover password</paper-button></a>
      </div>

      <style>
        .container {
          display: flex;
          flex-direction: column;
          max-width: 250px;
          margin: 0 auto;
        }

        .container a {
          text-decoration: none;
          color: var(--text-color);
          display: flex;
          flex-direction: column;
        } 
      </style>
    `;
  }

  login() {
    const mail = this.shadowRoot.getElementById('mail').value;
    const password = this.shadowRoot.getElementById('password').value;
    
    window.dispatchEvent(new CustomEvent('login', {detail: { mail: mail, password: password }}));
  }

  keyPressed(e){
    if(13 === e.keyCode) {
      this.login();
      this.shadowRoot.getElementById('login').click();
    }
  }
}

window.customElements.define('social-login', SocialLogin);
