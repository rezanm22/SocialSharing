﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Security.Claims;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using SocialSharing.Data.Users;

namespace SocialSharing.App.Authorization
{
    public class BearerAuthenticationHandler : AuthenticationHandler<BearerAuthenticationOptions>
    {
        public BearerAuthenticationHandler(IOptionsMonitor<BearerAuthenticationOptions> options, ILoggerFactory logger, UrlEncoder encoder, ISystemClock clock) : base(options, logger, encoder, clock) {}

        protected override Task<AuthenticateResult> HandleAuthenticateAsync()
        {
            Credentials credentials = new CredentialService().GetCredentials(Request);
            AuthenticateResult result = AuthenticateResult.Fail("Not logged in.");

            if (credentials != null)
            {
                User user = new UserService().FindByCredentials(credentials.Username, credentials.Password);

                if (user != null)
                {
                    ClaimsIdentity identity = new ClaimsIdentity(ClaimManager.GetClaims(user), Options.Scheme);
                    ClaimsPrincipal principal = new ClaimsPrincipal(identity);
                    AuthenticationTicket ticket = new AuthenticationTicket(principal, Options.Scheme);

                    result = AuthenticateResult.Success(ticket);
                }
            }

            return Task.FromResult(result);
        }
    }
}