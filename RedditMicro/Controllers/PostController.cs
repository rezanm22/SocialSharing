﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using MiniSocial;
using MiniSocial.Authentication;
using MiniSocial.Posts;

namespace SocialSharing.RedditMicro.Controllers
{
    [Route("api/posts")]
    public class PostController : Controller
    {
        [HttpGet("{query}")]
        public async Task<IEnumerable<IPost>> Get(string query)
        {
            return await new MiniPosts().GetPost(new PostRequest(Platforms.REDDIT, query));
        }
    }
}
