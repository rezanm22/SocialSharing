using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SocialSharing.Data.Sources;

namespace SocialSharing.Tests.Sources
{
    [TestClass]
    public class Get : SourceTest
    {
        [TestMethod]
        public void ById()
        {
            Source source = new Source()
            {
                Query = query,
                Platform = platform
            };

            CreatedSources.Add(Service.Create(source));

            Assert.IsNotNull(Service.Get(source.Id), "Source could not be found.");
        }

        [TestMethod]
        public void ByInvalidId()
        {
            Assert.IsNull(Service.Get(0), "A source could be found with an invalid id.");
        }

        [TestMethod]
        public void All()
        {
            Source source = new Source()
            {
                Query = query,
                Platform = platform
            };

            CreatedSources.Add(Service.Create(source));

            Assert.IsTrue(Service.GetAll().Any(), "No source could be found.");
        }
    }
}