namespace SocialSharing.App
{
    public class Services
    {
        public const string TWITTER = nameof(TWITTER);
        public const string TWITTER_URL = "http://localhost:5001/api";

        public const string FACEBOOK = nameof(FACEBOOK);
        public const string FACEBOOK_URL = "http://localhost:5002/api";

        public const string REDDIT = nameof(REDDIT);
        public const string REDDIT_URL = "http://localhost:5003/api";
    }
}