using Microsoft.VisualStudio.TestTools.UnitTesting;
using SocialSharing.Data.Users;

namespace SocialSharing.Tests.Users
{
    [TestClass]
    public class UserTests
    {
        [TestMethod]
        public void TestCreateUser()
        {
            string mail = "TestCreateUser@mail.com";
            CreateUser(mail);

            var service = new UserService();
            var foundUser = service.FindByMail(mail);

            Assert.IsTrue(foundUser != null, "user can't be saved or found");
        }

        [TestMethod]
        public void TestDeleteUser()
        {
            string mail = "TestDeleteUser@mail.com";
            CreateUser(mail);
            var service = new UserService();
            var foundUser = service.FindByMail(mail);

            service.Delete(foundUser.Mail);

            foundUser = service.FindByMail(mail);

            Assert.IsTrue(foundUser == null, "user isn't deleted");
        }

        [TestMethod]
        public void TestUpdateUser()
        {            
            string mail = "TestUpdateUser@mail.com";
            CreateUser(mail);
            var service =  new UserService();
            var foundUser = service.FindByMail(mail);

            foundUser.Theme = "dark";

            foundUser = service.Update(foundUser);

            Assert.IsTrue(foundUser.Theme == "dark", "Password isn't updated");
        }
        
        private void CreateUser(string mail)
        {
             var user = new User() {
                Mail = mail,
                Password = "Password123",
            };

            var service = new UserService();
            
            service.Create(user);
        }


       [TestMethod]
        public void CreateUserWithoutEmail()
        // Test passes if service returns null when no e-mail address is given during account creation
        {
            var user = new User() {
                    Mail = null,
                    Password = "testpassword",
            };

            var service = new UserService();

            Assert.IsNull(service.Create(user), "User was created without an e-mail address.");
        }

        [TestMethod]
        public void CreateUserWithoutPassword()
        // Test passes if service returns null when no password is given during account creation
        {
            var user = new User() {
                    Mail = "test@mail.com",
                    Password = null,
            };

            var service = new UserService();

            Assert.IsNull(service.Create(user), "User was created without a password");
        }
        
        [TestMethod]
        public void CreateUserAsNull()
        // Test passes if service returns null when no User object is passed during account creation.
        {
            var service = new UserService();
            Assert.IsNull(service.Create(null), "User was created without a valid user object.");
        }

        [TestMethod]
        public void CreateUserWithDuplicateEmail()
        // Test passes if service returns null when a user enters an already in-use e-mail address.
        {
            string mail = "test2@socialsharing.nl";
            string password = "testpassword";

            var user = new User() {
                Mail = mail,
                Password = password,
            };

            var service = new UserService();
            service.Create(user);

            Assert.IsNull(service.Create(user), "User was created with a duplicate e-mail address.");
        }

        [TestMethod]
        public void EditNonExistingUser()
        // Test passes if service returns null after trying to update a non-existing user.
        {
            string mail = "test38@socialsharing.nl";
            string password = "testpassword";

            var user = new User() {
                Mail = mail,
                Password = password,
            };
            var service = new UserService();
            Assert.IsNull(service.Update(user), "Service didn't return after attempting to edit a non-existing user.");
        }

        [TestMethod]
        public void EditNullUser()
        // Test passes if service returns null after trying to update a null user.
        {
            var service = new UserService();
            Assert.IsNull(service.Update(null),
            "Service didn't return after attempting to update a null user.");
        }

        [TestMethod]
        public void SetManager()
        {
            UserService service = new UserService();
            string mail = "test@test.test";
            CreateUser(mail);
            
            service.SetManager(mail, true);
            Assert.IsTrue(service.FindByMail(mail)?.IsManager ?? false, "Manager state ain't updated");
            
            service.SetManager(mail, false);
            Assert.IsFalse(service.FindByMail(mail)?.IsManager ?? true, "Manager state ain't updated");

            service.Delete(mail);
        }
    }
}
