import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import { SocialHeader } from './social-header';
import { SocialDrawer } from './social-drawer';
import { SocialUser } from './social-user';
import { SocialTheme } from './social-theme';
import { setRootPath } from '@polymer/polymer/lib/utils/settings.js';

import '@polymer/app-layout/app-layout.js';
import '@polymer/app-layout/app-scroll-effects/app-scroll-effects.js';
import '@polymer/iron-pages/iron-pages.js';
import '@polymer/app-route/app-location.js';
import '@polymer/app-route/app-route.js';
import '@polymer/paper-dialog';
import '@polymer/paper-toast';

setRootPath(Settings.rootPath);
class SocialApp extends PolymerElement {

  static get template() {
    return html`
      ${this.styles}
      <social-user></social-user>
      <social-theme></social-theme>
      <app-header-layout>
        <app-header slot="header" fixed effects="waterfall">
          <social-header></social-header>
        </app-header>

        <app-drawer-layout>
          <div class="content">
            <app-location route="{{route}}" url-space-regex="^[[rootPath]]">
            </app-location>

            <app-route route="{{route}}" pattern="[[rootPath]]:page" data="{{routeData}}" tail="{{subroute}}">
            </app-route>

            <iron-pages selected="[[page]]" attr-for-selected="name" role="main">
              <social-feed name="feed"></social-feed>
              <social-settings name="settings"></social-settings>
              <social-login name="login"></social-login>
              <social-sources name="sources"></social-sources>
              <social-managers name="managers"></social-managers>
              <social-register name="register"></social-register>
              <social-recover name="recover"></social-recover>
              <social-password-recovery name="passwordrecovery"></social-password-recovery>
            </iron-pages>
          </div>

          <app-drawer slot="drawer" opened="{{drawerOpened}}" swipe-open>
            <social-drawer></social-drawer>
          </app-drawer>
        </app-drawer-layout>
      </app-header-layout>

      <paper-toast id="toast"></paper-toast>
    `;
  }

  static get styles() {
    return html`
      <style>
        :host {
          display: block;
          background-color: var(--background-color);
        }        

        app-header {
          background-color: rgba(var(--background-color-values), var(--header-opacity));
          --app-header-shadow: {
            box-shadow: var(--header-shadow);
          };
        }

        social-feed {
          display: flex;
          flex-direction: column;
          margin: 0 auto;
        }

        @supports (backdrop-filter: blur(20px)) {
          app-header {
            background-color: rgba(var(--background-color-values), var(--header-opacity));
          }
        }
        
        @media (max-width: 640px) {
          app-drawer {
            --app-drawer-content-container: {
              background-color: transparent;
            }
          }

          .content {
            width: 100%;
            margin-left: 0;
          }

          social-feed {
            max-width: 100%;
          }
        }
      </style>
    `;
  }

  static get is() { return 'social-app'; }

  static get properties() {
    return {
      drawerOpened: {
        type: Boolean,
        value: false
      },
      page: {
        type: String,
        reflectToAttribute: true,
        observer: '_pageChanged'
      },
      routeData: Object,
      subroute: Object
    };
  }

  _toggleDrawer() {
    this.drawerOpened = !this.drawerOpened;
  }

  ready() {
    super.ready();
    this.addEventListener('drawer-toggle', () => this.drawerOpened = !this.drawerOpened);
    window.addEventListener('show-toast', (e) => this.showToast(e.detail.message));
  }

  showToast(message) {
    this.$.toast.setAttribute('text', message);
    this.$.toast.show();
  }

  static get observers() {
    return [
      '_routePageChanged(routeData.page)'
    ];
  }

  _routePageChanged(page) {
    if (!page) {
     this.page = 'feed';
   } else if (['feed', 'settings', 'sources', 'managers', 'login', 'register', 'recover', 'passwordrecovery'].indexOf(page) !== -1) {
     this.page = page;
   } else {
     this.page = 'view404';
   }
  }

  _pageChanged(page) {
    switch (page) {
      case 'feed':
        import('./social-feed.js');
        break;
      case 'settings':
        import('./social-settings.js');
        break;
      case 'sources':
        import('./social-sources.js');
        break;
      case 'managers':
        import('./social-managers.js');
        break;
      case 'login':
        import('./social-login.js');
        break;
      case 'register':
        import('./social-register');
        break;
      case 'recover':
        import('./social-recover');
        break;
      case 'passwordrecovery':
        import('./social-password-recovery');
        break;
      case 'view404':
        import('./my-view404.js');
        break;
    }
  }
}

window.customElements.define(SocialApp.is, SocialApp);