using System;
using Microsoft.AspNetCore.Authentication;

namespace SocialSharing.App.Authorization
{
    public static class AuthenticationBuilderExtensions
    {
        public static AuthenticationBuilder AddBearerAuthentication(this AuthenticationBuilder builder, Action<BearerAuthenticationOptions> options)
        {
            return builder.AddScheme<BearerAuthenticationOptions, BearerAuthenticationHandler>(BearerAuthenticationOptions.DefaultScheme, options);
        }
    }
}