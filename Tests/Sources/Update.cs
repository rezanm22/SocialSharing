using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SocialSharing.Data.Sources;

namespace SocialSharing.Tests.Sources
{
    [TestClass]
    public class Update : SourceTest
    {
        [TestMethod]
        public void NotChanged()
        {
            Source originalSource = Service.Create(new Source()
            {
                Query = query,
                Platform = platform
            });

            CreatedSources.Add(originalSource);

            Source newSource = new Source() 
            {
                Id = originalSource.Id,
                Query = originalSource.Query,
                Platform = originalSource.Platform
            };

            Assert.IsNotNull(Service.Update(newSource), "Valid source couldn't be updated.");
        }

        [TestMethod]
        public void NewQuery()
        {
            Source originalSource = Service.Create(new Source()
            {
                Query = query,
                Platform = platform
            });

            CreatedSources.Add(originalSource);

            Source newSource = new Source() 
            {
                Id = originalSource.Id,
                Query = originalSource.Query,
                Platform = originalSource.Platform
            };

            newSource.Query = "kanyewest";
            Assert.IsNotNull(Service.Update(newSource), "Valid source couldn't be updated.");
        }
        
        [TestMethod]
        public void NewPlatform()
        {
            Source originalSource = Service.Create(new Source()
            {
                Query = query,
                Platform = platform
            });

            CreatedSources.Add(originalSource);

            Source newSource = new Source() 
            {
                Id = originalSource.Id,
                Query = originalSource.Query,
                Platform = originalSource.Platform
            };

            newSource.Platform = Platform.FACEBOOK;
            Assert.IsNotNull(Service.Update(newSource), "Valid source couldn't be updated.");
        }
        
        [TestMethod]
        public void NewQueryAndPlatform()
        {
            Source originalSource = Service.Create(new Source()
            {
                Query = query,
                Platform = platform
            });

            CreatedSources.Add(originalSource);

            Source newSource = new Source() 
            {
                Id = originalSource.Id,
                Query = originalSource.Query,
                Platform = originalSource.Platform
            };

            newSource.Query = "kanyewest";
            newSource.Platform = Platform.FACEBOOK;
            Assert.IsNotNull(Service.Update(newSource), "Valid source couldn't be updated.");
        }
        
        [TestMethod]
        public void EmptyQuery()
        {
            Source originalSource = Service.Create(new Source()
            {
                Query = query,
                Platform = platform
            });

            CreatedSources.Add(originalSource);

            Source newSource = new Source() 
            {
                Id = originalSource.Id,
                Query = originalSource.Query,
                Platform = originalSource.Platform
            };

            newSource.Query = "";
            Assert.IsNull(Service.Update(newSource), "Invalid source could be updated.");
        }
        
        [TestMethod]
        public void NonExisting()
        {
            Source newSource = new Source() 
            {
                Id = 0,
                Query = query,
                Platform = platform
            };

            Assert.IsNull(Service.Update(newSource), "Non-exisiting source could be updated.");
        }
        
        [TestMethod]
        public void Null()
        {
            Assert.IsNull(Service.Update(null), "Non-exisiting source could be updated.");
        }
    }
}