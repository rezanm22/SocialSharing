namespace SocialSharing.App.Authorization
{
    public class ClaimType
    {
        public const string Mail = "Mail";
        public const string IsManager = "IsManager";
    }
}