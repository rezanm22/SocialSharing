using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;

namespace SocialSharing.Data
{
    internal abstract class Dao
    {   
        protected void Execute(Action<SocialSharingDatabase> action) {
            using (SocialSharingDatabase database = new SocialSharingDatabase())
            {
                action(database);
            }
        }
        
        protected T Execute<T>(Func<SocialSharingDatabase, T> action) {
            using (SocialSharingDatabase database = new SocialSharingDatabase())
            {
                return action(database);
            }
        }

        protected T FindEntity<T>(
            DbSet<T> collection, 
            Expression<Func<T, bool>> predicate) where T: class
        {
            return collection.FirstOrDefault(predicate);
        }

        protected IEnumerable<T> FindEntities<T>(
            DbSet<T> collection, 
            Expression<Func<T, bool>> predicate) where T: class
        {
            return collection.Where(predicate).ToList();
        }

        protected T AddOrUpdateEntity<T>(
            T entity, 
            DbSet<T> collection, 
            Expression<Func<T, bool>> predicate) where T: class
        {
            if (collection.Any(predicate)) collection.Update(entity);
            else collection.Add(entity);

            return entity;
        }

        protected void DeleteEntity<T, R>(
            T value, 
            DbSet<R> collection, 
            Expression<Func<R, bool>> predicate) where R: class
        {
            R entity = collection.FirstOrDefault(predicate);
            if (entity == null) return;
            collection.Remove(entity);
        }
    }
}