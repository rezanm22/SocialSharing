using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MiniConnector;
using MiniSocial;
using MiniSocial.Posts;
using SocialSharing.Data.Users;

namespace SocialSharing.App.Posts
{
    [Route("api/posts")]
    public class PostController : UserAwareController
    {
        // GET api/posts
        [AllowAnonymous]
        [HttpGet]
        public async Task<IEnumerable<IPost>> GetPosts()
        {
            return await new PostService().GetPosts(CurrentUser);
        }

        [HttpPut("like/{id}")]
        public async void LikePost(string id)
        {
            if(string.IsNullOrWhiteSpace(id)) return;
            try
            {
                if(id.StartsWith(Platforms.TWITTER))
                {
                    await MicroService.Call(Platforms.TWITTER, $"posts/like/{id}", "PUT");
                }
                else if(id.StartsWith(Platforms.FACEBOOK))
                {
                    await MicroService.Call(Platforms.FACEBOOK, $"posts/like/{id}", "PUT");
                }
            } catch (Exception e) {}
        }
    }
}