using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SocialSharing.Data.Sources;

namespace SocialSharing.Tests.Sources
{
    [TestClass]
    public class Create : SourceTest
    {
        [TestMethod]
        public void Valid()
        {
            Source source = new Source()
            {
                Query = query,
                Platform = platform
            };

            CreatedSources.Add(Service.Create(source));
            Assert.IsNotNull(CreatedSources.Last(), "Valid source couldn't be created.");
        }
        
        [TestMethod]
        public void Null()
        {
            CreatedSources.Add(null);
            Assert.IsNull(CreatedSources.Last(), "Null-object could be saved.");
        }
        
        [TestMethod]
        public void Empty()
        {
            Source source = new Source();

            CreatedSources.Add(Service.Create(source));
            Assert.IsNull(CreatedSources.Last(), "Invalid source could be created.");
        }
        
        [TestMethod]
        public void EmptyQuery()
        {
            Source source = new Source()
            {
                Platform = platform
            };

            CreatedSources.Add(Service.Create(source));
            Assert.IsNull(CreatedSources.Last(), "Invalid source could be created.");
        }
    }
}