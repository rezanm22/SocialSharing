import { LitElement, html } from '@polymer/lit-element';
export { SocialPasswordRecovery };

import '@polymer/paper-input/paper-input.js';
import '@polymer/paper-button/paper-button.js';
import '@polymer/iron-selector/iron-selector.js';
import '@polymer/app-route/app-route.js';

class SocialPasswordRecovery extends LitElement {

  static get properties() {
    return {};
  }

  _render({}) {
    return html`
      <div class="container">
      <paper-input label="password*" type="password" id="password" auto-validate pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).{6,256}$" error-message="Password must contain uppercase, lowercase and a number."></paper-input>
      <paper-input label="repeat password*" type="password" id="rewritten" on-keydown="${(e) => this.validatePassword(e)}" auto-validate pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).{6,256}$" error-message="Password must contain uppercase, lowercase and a number."></paper-input>
      <a name="recover password"><paper-button id="recover" on-click="${() => this.recoverPassword()}" disabled="true">reset password</paper-button></a>
      </div>
      <social-request id="recoverpassword" url="users/resetpassword" on-response="${() => this.onResponse()}" method="POST"><social-request>

      <style>
        .container {
          display: flex;
          flex-direction: column;
          max-width: 250px;
          margin: 0 auto;
        }

        .container a {
          text-decoration: none;
          color: var(--text-color);
          display: flex;
          flex-direction: column;
        } 
      </style>
    `;
  }

  recoverPassword() {
      var url = new URL(window.location.href);
      var id = url.searchParams.get("id");
      console.log(id);
      const request = this.shadowRoot.querySelector("#recoverpassword");
      const password = this.shadowRoot.getElementById('password').value;
      const rewritten = this.shadowRoot.getElementById('rewritten').value;

      request.body = {
        password: password,
        id: id
      };

      if( password == rewritten )
      {
        request.updateAndSend();
      } else {
        this.showPasswordError();
      }
    }

    showPasswordError() {
      window.dispatchEvent(new CustomEvent('show-toast', { detail: { message: 'Passwords did not match' }}));
      this.shadowRoot.getElementById('password').value = '';
      this.shadowRoot.getElementById('rewritten').value = '';
    }

    validatePassword(e) {
      const pass = this.shadowRoot.getElementById('password').value;
      const valid = this.isValidPassword(pass);
      this.shadowRoot.getElementById('recover').disabled = valid;
      if(this._confirmEnterButton(e) && valid === false) {
        this.shadowRoot.getElementById('recover').click();
      }
    }

    isValidPassword(password) {
      var pattern = "^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).{6,256}$";
      const matches = password.match(new RegExp(pattern));
      if(matches != null) { 
        return false;
      } else {
        return true;
      }
    }

    _confirmEnterButton(e){
      return e.keyCode === 13;
    }

    onResponse() {
      window.dispatchEvent(new CustomEvent('show-toast', { detail: { message: 'Password has been reset' }}));
    }
}

window.customElements.define('social-password-recovery', SocialPasswordRecovery);
