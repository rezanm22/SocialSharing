using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using SocialSharing.Data;

namespace SocialSharing.Data.Users
{
    internal class UserDao : Dao
    {
        public User FindByMail(string mail)
        {
            return Execute(database => FindEntity(database.Users, u => u.Mail == mail));
        }

        public User FindByCredentials(string mail, string password)
        {
            return Execute(database => FindEntity(database.Users, u => u.Mail == mail && u.Password == password));
        }

        public IEnumerable<User> GetAll()
        {
            return Execute(database => FindEntities(database.Users, u => true));
        }

        public User Save(User user)
        {
            if (string.IsNullOrWhiteSpace(user.Password))
            {
                user.Password = FindByMail(user.Mail)?.Password;
            }

            return Execute(database =>
            {
                AddOrUpdateEntity(user, database.Users, u => u.Mail == user.Mail);
                database.SaveChanges();
                return user;
            });
        }

        public void Delete(string mail)
        {
            Execute(database =>
            {
                DeleteEntity(mail, database.Users, u => u.Mail == mail);
                database.SaveChanges();
            });
        }
    }
}