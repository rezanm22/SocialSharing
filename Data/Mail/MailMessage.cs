using System.Collections.Generic;

namespace SocialSharing.Data.Mail
{
    public class MailMessage
    {
        
        public List<MailAddress> ToAddresses { get; private set; } = new List<MailAddress>();
        public List<MailAddress> FromAddresses { get; private set; } = new List<MailAddress>();
        public string Subject { get; set; }
        public string Content { get; set; }
    }
}