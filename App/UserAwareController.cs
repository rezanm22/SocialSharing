using System.Linq;
using Microsoft.AspNetCore.Mvc;
using SocialSharing.App.Authorization;
using SocialSharing.Data.Users;

namespace SocialSharing.App
{
    public abstract class UserAwareController : Controller
    {
        protected User CurrentUser => new UserService()
            .FindByMail(this.User.Claims
                .FirstOrDefault(c => c.Type == ClaimType.Mail)?.Value);
    }
}