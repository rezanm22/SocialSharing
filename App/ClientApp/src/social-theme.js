import { LitElement, html } from '@polymer/lit-element';
export { SocialTheme };

class SocialTheme extends LitElement {

  constructor() {
    super();
    this.setTheme();
    window.addEventListener('user', (e) => this.setTheme(e.detail.user != null ? e.detail.user.theme : null));
  }

  setTheme(theme = null) {
    document.body.className = theme;
  }

  static get properties() {
    return {};
  }

  _render({}) {
    return html``;
  }
}

window.customElements.define('social-theme', SocialTheme);
