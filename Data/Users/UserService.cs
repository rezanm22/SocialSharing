using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System;
using System.Text;
using System.Text.RegularExpressions;
using SocialSharing.Data.Mail;

namespace SocialSharing.Data.Users
{
    public class UserService
    {
        private const string salt = "wglgpO83C";
        private UserDao Dao = new UserDao();

        private Aes myAes = Aes.Create();

        public User FindByMail(string mail) => RemovePassword(Dao.FindByMail(mail));

        public User FindByCredentials(string mail, string password) => RemovePassword(Dao.FindByCredentials(mail, HashPassword(password)));

        public IEnumerable<User> GetAll() => Dao.GetAll().ToList().Select(u => RemovePassword(u));

        public User Create(User user)
        {
            if (!IsValidForCreation(user)) return null;
            user.Password = HashPassword(user.Password);
            return RemovePassword(Dao.Save(user));
        } 

        public User Update(User user) 
        {
            if (!IsValidForUpdate(user)) return null;
            if (!string.IsNullOrWhiteSpace(user.Password)) user.Password = HashPassword(user.Password);
            else user.Password = FindByMail(user.Mail)?.Password;
            return RemovePassword(Dao.Save(user));
        }

        private bool IsValidForCreation(User user)
        {
            return user != null &&
                !string.IsNullOrWhiteSpace(user.Mail) &&
                !string.IsNullOrWhiteSpace(user.Password);
        }

        private bool IsValidForUpdate(User user)
        {
            return user != null &&
                !string.IsNullOrWhiteSpace(user.Mail) &&
                FindByMail(user.Mail) != null;
        }

        private bool CreationAllowed(User user)
        {
            return IsValidForCreation(user) &&
                FindByMail(user.Mail) == null &&
                CheckMail(user.Mail) &&
                CheckPassword(user.Password);        
        }

        private bool CheckMail(string mail)
        {
            return new Regex("^(?(\")(\".+?(?<!\\)\"@)|(([0-9a-z]((\\.(?!\\.))|[-!#\\$%&'\\*\\+/=\\?\\^`\\{\\}\\|~\\w])*)(?<=[0-9a-z])@))(?(\\[)(\\[(\\d{1,3}\\.){3}\\d{1,3}\\])|(([0-9a-z][-\\w]*[0-9a-z]*\\.)+[a-z0-9][\\-a-z0-9]{0,22}[a-z0-9]))$").IsMatch(mail);
        }

        private bool CheckPassword(string password)
        {
            return password != null && new Regex("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d).{6,64}$").IsMatch(password);
        }

        public void Delete(string mail) => Dao.Delete(mail);

        public void SendRecoveryEmail(string mail, string host)
        {
            string saltymail = mail + salt;
            if (mail == null || FindByMail(mail) == null) return;

            var user = FindByMail(mail);

            MailAddress fromMail = new MailAddress();
            fromMail.Name = MailService.MailConfiguration.SmtpSenderName;
            fromMail.Mail = MailService.MailConfiguration.SmtpUsername;

            MailAddress toMail = new MailAddress();
            toMail.Name = user.Name;
            toMail.Mail = mail;


            MailMessage message = new MailMessage();
            message.ToAddresses.Add(toMail);
            message.FromAddresses.Add(fromMail);
            message.Subject = "Password Recovery";

            message.Content =  $"Dear {user.Name},\r\n\r\nThis is the link to reset your password: https://{host}/passwordrecovery?id={Convert.ToBase64String(Encoding.UTF8.GetBytes(saltymail))}";
            MailService service = new MailService();

            service.Send(message);

            
            // call mailservice
        }

        public void ResetPassword(string id, string password)
        {
            if(!CheckPassword(password)) return;
            
            byte[] mail = Convert.FromBase64String(id);
            string mailaddress = Encoding.UTF8.GetString(mail);
            mailaddress = mailaddress.Remove(mailaddress.Length - salt.Length);
            User user = Dao.FindByMail(mailaddress);
            if (user == null) return;
            user.Password = HashPassword(password);
            Dao.Save(user);
        }

        private string HashPassword(string password) => HashService.Hash(password);

        private User RemovePassword(User user)
        {
            if (user == null) return null;

            user.Password = null;
            return user;
        }

        public void SetManager(string mail, bool isManager)
        {
            User user = FindByMail(mail);
            if (user == null) return;

            user.IsManager = isManager;
            Dao.Save(user);
        }
    }
}