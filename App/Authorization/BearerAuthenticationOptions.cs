﻿using Microsoft.AspNetCore.Authentication;

namespace SocialSharing.App.Authorization
{
    public class BearerAuthenticationOptions : AuthenticationSchemeOptions
    {
        public const string DefaultScheme = "Bearer";
        public string Scheme => DefaultScheme;
    }
}