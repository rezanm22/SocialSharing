using System.Collections.Generic;
using System;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SocialSharing.Data.Mail;
using SocialSharing.Data.Users;

namespace SocialSharing.App.Users
{
    [Route("api/users")]
    public class UserController : UserAwareController
    {
        private UserService Service = new UserService();

        // GET api/users/me
        [AllowAnonymous]
        [HttpGet("me")]
        public User GetCurrentUser()
        {
            return CurrentUser;
        }

        // GET api/users/{mail}
        [HttpGet("{mail}")]
        public User GetUser(string mail)
        {
            return Service.FindByMail(mail);
        }

        // POST api/users
        [AllowAnonymous]
        [HttpPost]
        public User Create([FromBody] User user)
        {
            return Service.Create(user);
        }

        // PUT api/users
        [HttpPut]
        public User Update([FromBody] User user)
        {
            return Service.Update(user);
        }

        // DELETE api/users/{mail}
        [HttpDelete("{mail}")]
        public void Delete(string mail)
        {
            Service.Delete(mail);
        }

        // PUT api/users/manager/{mail}/{isManager}
        [Authorize(Policy = Policy.IsManager)]
        [HttpPut("manager/{mail}/{isManager}")]
        public void SetManager(string mail, bool isManager)
        {
            Service.SetManager(mail, isManager);
        }

        // GET api/users
        [Authorize(Policy = Policy.IsManager)]
        [HttpGet]
        public IEnumerable<User> GetAll()
        {
            return Service.GetAll();
        }

        // GET api/users/recover/{mail}
        [AllowAnonymous]
        [HttpGet("recover/{mail}")]
        public void SendRecoveryEmail(string mail)
        {
            string host = HttpContext.Request.Host.Host;
            new UserService().SendRecoveryEmail(mail, host);
        }

        // POST api/users/resetpassword
        [AllowAnonymous]
        [HttpPost("resetpassword")]
        public void ResetPassword([FromBody] PasswordResetRequest request)
        {
            new UserService().ResetPassword(request.Id, request.Password);
        }
    }
}