using System.Collections.Generic;

namespace SocialSharing.Data.Mail
{
    public interface IMailService
    {
        void Send(MailMessage mailMessage);
        List<MailMessage> ReceiveEmail(int maxCount = 10);
    }
}