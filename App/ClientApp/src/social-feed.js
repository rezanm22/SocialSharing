import { LitElement, html } from '@polymer/lit-element';
import { repeat } from 'lit-html/lib/repeat';
export { SocialFeed };

import { SocialRequest} from './social-request';
import { SocialPost} from './social-post';

class SocialFeed extends LitElement {

  constructor() {
    super();

    this.posts = [];
  }

  static get properties() {
    return {
      posts: Object
    };
  }

  _render({posts}) {
    return html`
      <social-request auto url="posts" on-response="${e => this.onResponse(e)}"></social-request>

      ${posts.length == 0 ? html`<div class="empty-text">Nothing to show here...</div>` : ''}
      <div class="masonry">
        ${
          repeat(posts, post => post, post => html`
            <div class="item">
              <social-post post="${post}" on-rendered="${() => { this.requestMasonryUpdate(); this.requestMasonryUpdate() }}"></social-post>
            </div>
          `)
        }
      </div>

      <style>
        :host {
          padding: 0 var(--spacing-small);
        }

        .empty-text {
          text-align: center;
        }

        .masonry {
          column-gap: 0;
          column-count: 1;
        }

        .item {
          display: inline-block;
          width: 100%;
        }

        @media only screen and (min-width: 944px) {
          .masonry {
            column-count: 2;
          }
        }

        @media only screen and (min-width: 1244px) {
          .masonry {
            column-count: 3;
          }
        }

        @media only screen and (min-width: 1544px) {
          .masonry {
            column-count: 4;
          }
        }

        @media only screen and (min-width: 1844px) {
          .masonry {
            column-count: 5;
          }
        }

        @media only screen and (min-width: 2144px) {
          .masonry {
            column-count: 6;
          }
        }
      </style>
    `;
  }

  onResponse(event) {
    this.posts = event.detail.response;
  }
}

window.customElements.define('social-feed', SocialFeed);
