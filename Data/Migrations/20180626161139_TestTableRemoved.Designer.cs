﻿// <auto-generated />
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.EntityFrameworkCore.Storage.Internal;
using SocialSharing.Data;
using SocialSharing.Data.Sources;
using System;

namespace Data.Migrations
{
    [DbContext(typeof(SocialSharingDatabase))]
    [Migration("20180626161139_TestTableRemoved")]
    partial class TestTableRemoved
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn)
                .HasAnnotation("ProductVersion", "2.0.2-rtm-10011");

            modelBuilder.Entity("SocialSharing.Data.Sources.Source", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("Platform");

                    b.Property<string>("Query");

                    b.HasKey("Id");

                    b.ToTable("Sources");
                });

            modelBuilder.Entity("SocialSharing.Data.Users.User", b =>
                {
                    b.Property<string>("Mail")
                        .ValueGeneratedOnAdd();

                    b.Property<bool>("IsManager");

                    b.Property<string>("Name");

                    b.Property<string>("Password");

                    b.Property<string>("Theme");

                    b.HasKey("Mail");

                    b.ToTable("Users");
                });
#pragma warning restore 612, 618
        }
    }
}
