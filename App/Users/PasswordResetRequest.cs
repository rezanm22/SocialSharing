using Newtonsoft.Json;

namespace SocialSharing.App.Users
{
    public class PasswordResetRequest
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("password")]
        public string Password { get; set; }
    }
}