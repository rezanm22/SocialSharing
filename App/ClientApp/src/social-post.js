import { LitElement, html } from '@polymer/lit-element';
export { SocialPost };

import '@polymer/iron-icon';
import '@polymer/paper-card';
import '@polymer/paper-button';
import '@polymer/paper-dialog';
import '@polymer/paper-input/paper-input.js';


class SocialPost extends LitElement {

  constructor() {
    super();

    this.post = {};
  }

  static get properties() {
    return {
      post: Object
    };
  }

  _render({post}) {
    return html`
      <paper-card on-click="${() => window.open(post.link)}">
        ${post.picture ? html`<img src="${post.picture}">` : ''}
        <div class="info-bar">
          ${post.author} - ${this.getDate()}
          <ion-icon name="${this.getIcon(post.platform)}"></ion-icon>
        </div>
        <div class="content">
          ${post.message}
        </div>

        <paper-dialog id="share" style='max-width: 60%; min-width: 40%; margin-left:60%;'>
          
        <paper-input placeholder='Enter your message'></paper-input>
        
        <div class="buttons">
          <paper-button dialog-confirm onclick="${() => this.openShareLink()}" autofocus>Share this post</paper-button>
        </div>
      </paper-dialog>

      </paper-card>
      <social-request id="request" url="posts/like/${encodeURI(post.universal_id)}" method="PUT"></social-request>

      <style>
        :host {
          display: flex;
          padding: var(--spacing-small) var(--spacing-small);
        }

        paper-card {
          width: 100%;
          overflow: hidden;
          background-color: transparent;
          border: var(--post-border);
          cursor: pointer;
        }

        img {
          width: 100%;
          */ max-height: var(--post-max-header-height); */
          object-fit: cover;
        }

        .content {
          margin: var(--spacing);
          overflow: hidden;
          text-overflow: ellipsis;
          display: -webkit-box;
          line-height: var(--font-size);
          max-height: calc(var(--font-size) * var(--preview-lines));
          -webkit-line-clamp: var(--post-preview-lines);
          -webkit-box-orient: vertical;      
        }

        .info-bar {
          color: var(--text-color-secondary);
          padding: var(--spacing) var(--spacing) 0 var(--spacing);
        }

        .info-bar ion-icon {
          float: right;
          font-size: var(--text-size);
          width: var(--text-size);
          height: var(--text-size);
          fill: var(--text-color-secondary);
        }

        .action-bar {
          display: flex;
          border-top: 1px solid var(--post-border-color);
          padding: var(--spacing-tiny);
        }
        
        .action-bar > * {
          flex: 1 0 0;
          text-align: center;
          text-transform: uppercase;
          color: var(--text-color-secondary);
        }
      </style>
    `;
  }

  openShareDialog(){
    this.shadowRoot.querySelector('#share').toggle();
  }

  getDate() {
    return new Date(Date.parse(this.post.creation_date)).toLocaleDateString();
  }

  likePost() {
    this.shadowRoot.getElementById('request').send();
  }

  getIcon(platform){
    switch(platform){
      case 'TWITTER':
        return 'logo-twitter';
        break;
      case 'FACEBOOK':
        return 'logo-facebook';
        break;
      case 'LINKEDIN':
        return 'logo-linkedin';
        break;
      case 'INSTAGRAM':
        return 'logo-instagram';
        break;
      case 'REDDIT':
        return 'logo-reddit';
        break;
    }
  }
}

window.customElements.define('social-post', SocialPost);
