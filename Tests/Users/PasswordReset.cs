using System;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SocialSharing.Data.Users;

namespace SocialSharing.Tests.Users
{
    [TestClass]
    public class PasswordReset : UserTest
    {
        [TestMethod]
        public void Valid()
        {
            User user = new User()
            {
                Mail = "testemail@unit.com",
                Name = "Unit Test",
                Password = "Test1234"
            };

            CreatedUsers.Add(Service.Create(user));

            string saltymail = user.Mail + "wglgpO83C";
            Service.ResetPassword(Convert.ToBase64String(Encoding.UTF8.GetBytes(saltymail)), "Unit5678");
            Assert.IsNotNull(Service.FindByCredentials(user.Mail, "Unit5678"), "Something went wrong during the password change");
        }

        [TestMethod]
        public void Null()
        {
            User user = new User()
            {
                Mail = "testemail@unit.com",
                Name = "Unit Test",
                Password = "Test1234"
            };
            
            CreatedUsers.Add(Service.Create(user));
            string saltymail = user.Mail + "wglgpO83C";
            Service.ResetPassword(Convert.ToBase64String(Encoding.UTF8.GetBytes(saltymail)), null);
            Assert.IsNotNull(Service.FindByCredentials(user.Mail, "Test1234"));
        }

        [TestMethod]
        public void NotComplex()
        {
            User user = new User()
            {
                Mail = "testemail@unit.com",
                Name = "Unit Test",
                Password = "Test1234"
            };
            
            CreatedUsers.Add(Service.Create(user));
            string saltymail = user.Mail + "wglgpO83C";
            Service.ResetPassword(Convert.ToBase64String(Encoding.UTF8.GetBytes(saltymail)), "kaas1234");
            Assert.IsNotNull(Service.FindByCredentials(user.Mail, "Test1234"));
        }

        [TestMethod]
        public void Short()
        {
            User user = new User()
            {
                Mail = "testemail@unit.com",
                Name = "Unit Test",
                Password = "Test1234"
            };
            
            CreatedUsers.Add(Service.Create(user));
            string saltymail = user.Mail + "wglgpO83C";
            Service.ResetPassword(Convert.ToBase64String(Encoding.UTF8.GetBytes(saltymail)), "Ks$.2");
            Assert.IsNotNull(Service.FindByCredentials(user.Mail, "Test1234"));

        }
    }
}