import { LitElement, html } from '@polymer/lit-element';
export { SocialRequest };

import '@polymer/iron-ajax';

class SocialRequest extends LitElement {

  constructor() {
    super();

    this.method = 'GET';
    this.response = null;
    this.body = null;
    this.auto = false;
    this.sendAfterRender = false;
  }

  static get properties() {
    return {
      url: String,
      method: String,
      body: Object,
      auto: Boolean
    };
  }

  _render({url, method, body, auto}) {
    return html`
      <iron-ajax
        auto="${auto}"
        url="${window.Settings.apiRootPath}${url}"
        body="${body}"
        headers="${this.getHeaders()}"
        handle-as="json"
        content-type="application/json"
        method="${method}"
        on-response="${e => this.onResponse(e)}"></iron-ajax>
    `;
  }

  _didRender() {
    if (this.sendAfterRender) {
      this.send();
      this.sendAfterRender = false;
    }
  }

  update() {
    this._requestRender();
    return this;
  }

  updateAndSend() {
    this.sendAfterRender = true;
    this.update();
  }

  send() {
    this.shadowRoot.querySelector('iron-ajax').generateRequest();
  }

  onResponse(event) {
    this.response = event.detail.response;
  }

  getHeaders() {
    let result = {};
    let token = localStorage.getItem('token');
    if (token) {
      result = { Authorization: `Bearer ${token}` };
    }

    return result;
  }
}

window.customElements.define('social-request', SocialRequest);
