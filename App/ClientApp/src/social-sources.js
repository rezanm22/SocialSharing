import { LitElement, html } from '@polymer/lit-element';
import { repeat } from 'lit-html/lib/repeat';
export { SocialSources };

import '@polymer/paper-input/paper-input.js';
import '@polymer/paper-ripple';

class SocialSources extends LitElement {

  constructor() {
    super();

    this.sources = [];
    this.platforms = [];
    this.newSource = {
      query: null,
      platform: null
    };
  }

  static get properties() {
    return {
      sources: Object,
      platforms: Object,
      newSource: Object
    };
  }

  _render({ sources, platforms, newSource }) {
    return html`
      <div>
        <social-request id="getSources" auto url="sources" on-response="${e => this.onSourcesResponse(e)}"></social-request>
        <social-request auto url="sources/platforms" on-response="${e => this.onPlatformsResponse(e)}"></social-request>
        <social-request id="createSource" url="sources" method="POST" on-response="${() => this.shadowRoot.getElementById('getSources').send()}"></social-request>
        <social-request id="updateSource" url="sources" method="PUT"></social-request>
        <social-request id="deleteSource" url="sources" method="DELETE"></social-request>

        All changes will be saved automatically.

        ${
          repeat(sources, source => source, source => html`
            <div id="source-${source.id}" class="item">
              <paper-input value="${source.query}" label="query" on-blur="${(e) => this.updateQuery(source, e.target.value)}"></paper-input>
              <div class="icons">
                ${
                  repeat(platforms, platform => platform, platform => html`
                    <div id="icon-${platform}" class="icon">
                      <ion-icon name="logo-${platform.toLowerCase()}" on-click="${() => this.updatePlatform(source, platform)}"></ion-icon>
                      <paper-ripple class="circle" recenters></paper-ripple>
                    </div>
                  `)
                }
              </div>
              <paper-icon-button icon="delete" on-click="${() => this.deleteSource(source)}"></paper-icon-button>
            </div>
          `)
        }

        <div class="item new">
          <paper-input id="newQuery" value="${newSource.query}" label="query" on-blur="${(e) => this.updateNewQuery(e.target.value)}"></paper-input>
          <div class="icons">
            ${
              repeat(platforms, platform => platform, platform => html`
                <div id="icon-${platform}" class="icon">
                  <ion-icon name="logo-${platform.toLowerCase()}" on-click="${() => this.updateNewPlatform(platform)}"></ion-icon>
                  <paper-ripple class="circle" recenters></paper-ripple>
                </div>
              `)
            }
          </div>
          <paper-icon-button icon="save" on-click="${() => this.createNewSource()}"></paper-icon-button>
        </div>
      </div>
      
      <style>
        :host {
          display: flex;
          flex-direction: column;
          align-items: center;
        }

        .item {
          display: flex;
          flex-direction: row;
          align-items: center;
        }

        .item > * {
          display: flex;
        }

        .item > *:last-child {
          margin-left: var(--spacing);
        }

        .icons {
          display: flex;
          flex-direction: row;
          border: 1px solid var(--text-color-secondary);
          border-radius: var(--text-size);
          padding: 0 5px;
          margin-left: var(--spacing-large);
        }

        .icon {
          width: calc(2 * var(--text-size));
          height: calc(2 * var(--text-size));
          position: relative;
          display: inline-block;
        }

        .icon ion-icon {
          margin: calc(.25 * var(--text-size));
          width: calc(1.5 * var(--text-size));
          height: calc(1.5 * var(--text-size));
          fill: var(--text-color-secondary);
        }

        .icon.active ion-icon {
          fill: var(--primary-color);
        }
      </style>
    `;
  }

  _didRender() {
    this.updatePlatforms();
  }

  updatePlatforms() {
    for (const source of this.sources) {
      const lastActive = this.shadowRoot.querySelector(`#source-${source.id} .active`);
      if (lastActive != null) {
        lastActive.classList.remove('active');
      }

      const active = this.shadowRoot.querySelector(`#source-${source.id} #icon-${source.platform}`);
      if (active != null) {
        active.classList.add('active');
      }
    }
    
    const lastActive = this.shadowRoot.querySelector(`.item.new .active`);
    if (lastActive != null) {
      lastActive.classList.remove('active');
    }
    
    const active = this.shadowRoot.querySelector(`.item.new #icon-${this.newSource.platform}`);
    if (active != null) {
      active.classList.add('active');
    }
  }

  updateQuery(source, query) {
    if (source.query == query) return;
    source.query = query;
    this.updateSource(source);
  }

  updatePlatform(source, platform) {
    if (source.platform == platform) return;
    source.platform = platform;
    this.updatePlatforms();
    this.updateSource(source);
  }

  updateSource(source) {
    const request = this.shadowRoot.getElementById('updateSource');
    request.body = source;
    request.updateAndSend();
  }

  updateNewQuery(query) {
    this.newSource.query = query;
  }

  updateNewPlatform(platform) {
    this.newSource.platform = platform;
    this.updatePlatforms();
  }

  createNewSource() {
    if (this.newSource.query == null || this.newSource.query.length == 0 || this.newSource.platform == null) return;
    const request = this.shadowRoot.getElementById('createSource');
    request.body = this.newSource;
    request.updateAndSend();
    this.sources.push(this.newSource);
    this.newSource = {
      query: null,
      platform: null
    };
    this.shadowRoot.getElementById('newQuery').value = '';
  }

  deleteSource(source) {
    const request = this.shadowRoot.getElementById('deleteSource');
    request.url = `sources/${source.id}`;
    request.updateAndSend();

    this.sources = this.sources.filter(s => s.id != source.id);
  }

  onSourcesResponse(event) {
    this.sources = event.detail.response;
  }

  onPlatformsResponse(event) {
    this.platforms = event.detail.response;
  }
}

window.customElements.define('social-sources', SocialSources);
