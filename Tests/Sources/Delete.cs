using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SocialSharing.Data.Sources;

namespace SocialSharing.Tests.Sources
{
    [TestClass]
    public class Delete : SourceTest
    {
        [TestMethod]
        public void Valid()
        {
            Source source = new Source()
            {
                Query = query,
                Platform = platform
            };

            CreatedSources.Add(Service.Create(source));
            Service.Delete(source.Id);

            Assert.IsNull(Service.Get(source.Id), "A deleted source could be found.");
        }
        
        [TestMethod]
        public void Invalid()
        {
            Service.Delete(0);

            Assert.IsNull(Service.Get(0), "A deleted source could be found.");
        }
    }
}