import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
export { SocialHeader };

import '@polymer/app-layout/app-toolbar/app-toolbar.js';
import '@polymer/paper-icon-button/paper-icon-button.js';
import '@polymer/iron-icons/iron-icons.js';

class SocialHeader extends PolymerElement {
  static get template() {
    return html`
      ${this.styles}

      <app-toolbar>
        <div main-title><a href="/"><div class="logo"></div></a></div>
        <paper-icon-button icon="menu" class="drawer-toggle" on-click="toggleDrawer"></paper-icon-button>
      </app-toolbar>
    `;
  }

  static get styles() {
    return html`
      <style>
        .logo {
          width: 200px;
          height: var(--logo-height);
          background-image: var(--logo-url);
          background-size: contain;
          background-position: 50% 50%;
          display: inline-block;
          background-repeat: no-repeat;
        }

        .a {
          width: 100%;
          text-align:center;
        }

        [main-title] {
          position: absolute;
          left: 0;
          right: 0;
          display: flex;
          justify-content: center;
          pointer-events: visible !important;
        }

        .drawer-toggle {
          display: none;
        }

        @supports (backdrop-filter: blur(20px)) {
          app-toolbar {
            -webkit-backdrop-filter: blur(var(--header-blur));
          }
        }
        
        @media (max-width: 640px) {
          .drawer-toggle {
            display: block;
          }
        }
      </style>
    `;
  }
  
  static get is() { return 'social-header'; }

  toggleDrawer() {
    this.dispatchEvent(new CustomEvent('drawer-toggle', { bubbles: true, composed: true }));
  }
}

window.customElements.define(SocialHeader.is, SocialHeader);
