import { LitElement, html } from '@polymer/lit-element';
import { SocialRequest } from './social-request';
export { SocialUser };

class SocialUser extends LitElement {

  constructor() {
    super();

    window.user = null;

    window.addEventListener('update-user', () => this.updateUser());
    window.addEventListener('login', (e) => this.login(e.detail.mail, e.detail.password));
    window.addEventListener('logout', () => this.logout());
  }

  static get properties() {
    return {};
  }

  _render({ }) {
    return html`
      <social-request auto id="user" url="users/me" on-response="${(e) => this.onResponse(e)}"><social-request>
    `;
  }

  onResponse(e) {
    const response = e.detail.response;
    window.user = response;
    
    window.dispatchEvent(new CustomEvent('user', { bubbles: true, detail: { user: response }}));
    
    if (response) {
      this.showLoginMessage();
    } else if (!response && localStorage.getItem('token')) {
      localStorage.removeItem('token');
      this.showInvalidCredentialsMessage();
    }
  }

  updateUser() {
    this.shadowRoot.getElementById('user').updateAndSend();
  }

  login(mail, password) {
    const token = btoa(`${mail}:${password}`);
    localStorage.setItem('token', token);
    this.updateUser();
  }

  logout() {
    localStorage.removeItem('token');
    this.updateUser();
    this.showLogoutMessage();
  }

  showLoginMessage() {
    window.dispatchEvent(new CustomEvent('show-toast', { detail: { message: 'Welcome back :)' }}));
  }

  showLogoutMessage() {
    window.dispatchEvent(new CustomEvent('show-toast', { detail: { message: 'Bye bye' }}));
  }

  showInvalidCredentialsMessage() {
    window.dispatchEvent(new CustomEvent('show-toast', { detail: { message: 'Your credentials are invalid' }}));
  }
}

window.customElements.define('social-user', SocialUser);
