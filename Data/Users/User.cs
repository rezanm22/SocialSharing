using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace SocialSharing.Data.Users
{
    public class User
    {
        [Key]
        [JsonProperty("mail")]
        public string Mail { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("password")]
        public string Password { get; set; }

        [JsonProperty("is_manager")]
        public bool IsManager { get; set; }

        [JsonProperty("theme")]
        public string Theme { get; set; }
    }
}