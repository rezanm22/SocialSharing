using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SocialSharing.Data.Sources;

namespace SocialSharing.App.Sources
{
    [Route("api/sources")]
    public class SourcesController : UserAwareController
    {
        // GET api/sources
        [AllowAnonymous]
        [HttpGet]
        public IEnumerable<Source> Get()
        {
            return new SourceService().GetAll().OrderBy(s => s.Id);
        }

        // POST api/sources
        [Authorize(Policy = Policy.IsManager)]
        [HttpPost]
        public Source Create([FromBody] Source source)
        {
            return new SourceService().Create(source);
        }

        // PUT api/sources
        [Authorize(Policy = Policy.IsManager)]
        [HttpPut]
        public Source Update([FromBody] Source source)
        {
            return new SourceService().Update(source);
        }

        // DELETE api/sources/{id}
        [Authorize(Policy = Policy.IsManager)]
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            new SourceService().Delete(id);
        }

        // GET api/sources/platforms
        [Authorize(Policy = Policy.IsManager)]
        [HttpGet("platforms")]
        public IEnumerable<string> GetPlatforms()
        {
            return new SourceService().GetPlatforms();
        }
    }
}