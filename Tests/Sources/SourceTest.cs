using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SocialSharing.Data.Sources;

namespace SocialSharing.Tests.Sources
{
    public abstract class SourceTest
    {
        protected const string query = "realdonaldtrump";
        protected const Platform platform = Platform.TWITTER;
        protected List<Source> CreatedSources;
        protected SourceService Service;
        
        [TestInitialize]
        public void Init()
        {
            Service = new SourceService();
            CreatedSources = new List<Source>();
        }

        [TestCleanup]
        public void Teardown()
        {
            CreatedSources
                .Where(s => s != null)
                .ToList()
                .ForEach(s => Service.Delete(s.Id));
        }
    }
}