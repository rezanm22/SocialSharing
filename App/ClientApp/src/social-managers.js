import { LitElement, html } from '@polymer/lit-element';
import { repeat } from 'lit-html/lib/repeat';
export { SocialManagers };

import '@polymer/paper-toggle-button/paper-toggle-button.js';
import '@polymer/paper-ripple';

class SocialManagers extends LitElement {

  constructor() {
    super();

    this.users = [];
  }

  static get properties() {
    return {
      users: Object
    };
  }

  _render({ users }) {
    return html`
      <div>
        <social-request auto url="users" on-response="${e => this.onUsersResponse(e)}"></social-request>
        <social-request id="managerRequest" url="users/manager" method="PUT"></social-request>

        All changes will be saved automatically.

        <table>
          <thead>
            <tr>
              <td>name</td>
              <td>mail</td>
              <td>manager</td>
            </tr>
          </thead>
        ${
          repeat(users, user => user, user => html`
            <tr>
              <td>${user.name}</td>
              <td>${user.mail}</td>
              <td><paper-toggle-button id="user-toggle-${user.mail.replace(/\W/g, '')}" on-click="${e => this.setManager(user, e)}"></paper-toggle-button></td>
            </tr>
          `)
        }
        <table>
      </div>

      <style>
        :host {
          display: flex;
          flex-direction: column;
          align-items: center;
        }

        td {
          padding: 0 var(--spacing);
          padding-bottom: var(--spacing-small);
        }

        thead td {
          text-align: center;
          font-weight: bold;
        }

        td:last-of-type {
          display: flex;
          flex-direction: column;
          align-items: center;
        }
      </style>
    `;
  }

  _didRender() {
    this.users
      .filter(u => u.is_manager)
      .forEach(u => this.shadowRoot.querySelector(`#user-toggle-${u.mail.replace(/\W/g, '')}`).setAttribute('checked', ''));
  }

  onUsersResponse(event) {
    this.users = event.detail.response
      .filter(u => u.mail != window.user.mail)
      .sort((a, b) => a.name < b.name ? -1 : 1);
  }

  setManager(user, event) {
    const request = this.shadowRoot.querySelector('#managerRequest');
    request.url = `users/manager/${user.mail}/${event.target.checked}`;
    request.updateAndSend();
  }
}

window.customElements.define('social-managers', SocialManagers);