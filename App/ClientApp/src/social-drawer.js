import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-selector/iron-selector.js';
import '@polymer/paper-checkbox/paper-checkbox.js';

export { SocialDrawer };

class SocialDrawer extends PolymerElement {
  constructor() {
    super();
    this.setName();
    window.addEventListener('user', (e) => this.setName(e.detail.user));
  }

  setName(user = null) {
    this.set('user', user != null ? 
      user.name :
      'unknown');
    this.set('loggedIn', user != null);
    this.set('isAdmin', user != null && user.is_manager);
    
  }

  static get template() {
    return html`
      ${this.styles}

      <div class="drawer-content">
        <h2 hidden$="[[!loggedIn]]">[[user]]</h2>
        <iron-selector selected="[[page]]" attr-for-selected="name" class="content" role="navigation">
          <a name="login" href="[[rootPath]]login" hidden$="[[loggedIn]]">Login</a>
          <a name="feed" href="[[rootPath]]feed">Feed</a>
          <a name="settings" href="[[rootPath]]settings">Settings</a>
          <a name="sources" href="[[rootPath]]sources" hidden$="[[!isAdmin]]">Sources</a>
          <a name="managers" href="[[rootPath]]managers" hidden$="[[!isAdmin]]">Managers</a>
          <span on-click="logout" hidden$="[[!loggedIn]]">Logout</span>
        </iron-selector>
      </div>
    `;
  }

  logout() {
    window.dispatchEvent(new CustomEvent('logout'));
    window.location.href = '/login';
  }

  static get styles() {
    return html`
      <style>
        .drawer-content {
          margin-top: var(--header-height);
          height: calc(100% - var(--header-height));
          padding: var(--spacing);
          background-color: var(--drawer-color);
        }

        .drawer-content a,
        .drawer-content span {
          color: var(--text-color);
          text-decoration: none;  
          padding-left: var(--spacing);
          padding-top: 0.5rem;
          cursor: pointer;
        }

        .drawer-content a:not([hidden]),
        .drawer-content span:not([hidden]) {
          display: block;
        }

        h2 {
          font-weight: normal;
          padding-left: var(--spacing);
        }

        h3 {
          font-weight: normal;
          padding-left: var(--spacing);
          padding-top: var(--spacing);
          margin-bottom: 0;
        }

        ul {
          padding-left: 0;
        }

        ul li {
          list-style: none;
          padding: var(--spacing-tiny) var(--spacing);
        }
      </style>
    `;
  }

  static get properties() {
    return {
      user: {
        type: Object
      },
      loggedIn: {
        type: Boolean,
        value: false
      },
      isAdmin: {
        type: Boolean,
        value: false
      }
    };
  }

  static get is() { return 'social-drawer'; }
}

window.customElements.define(SocialDrawer.is, SocialDrawer);
