using System;
using System.Collections.Generic;

namespace SocialSharing.Data.Sources
{
    public class SourceService
    {
        private SourceDao Dao = new SourceDao();

        public Source Get(int id) => Dao.Get(id);

        public IEnumerable<Source> GetAll() => Dao.GetAll();

        public Source Create(Source source)
        {
            if (source == null ||
                string.IsNullOrWhiteSpace(source.Query))
            {
                return null;
            }

            return Dao.Save(source);
        }

        public Source Update(Source source)
        {
            if (source == null ||
                source.Id <= 0 ||
                string.IsNullOrWhiteSpace(source.Query))
            {
                return null;
            }

            return Dao.Save(source);
        }

        public void Delete(int id) => Dao.Delete(id);

        public IEnumerable<string> GetPlatforms()
        {
            foreach (Platform platform in Enum.GetValues(typeof(Platform)))
            {
                yield return platform.ToString();
            }
        }
    }
}